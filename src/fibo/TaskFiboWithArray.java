package fibo;

import commons.Task;

import java.math.BigInteger;

public class TaskFiboWithArray implements Task {
    @Override
    public String run(String[] data) {
        return fibo(Integer.parseInt(data[0])).toString();
    }

    private BigInteger fibo(int value) {
        if (value == 0 || value == 1) {
            return BigInteger.valueOf(value);
        }

        BigInteger[] bigIntegers = new BigInteger[value + 1];
        bigIntegers[0] = BigInteger.ZERO;
        bigIntegers[1] = BigInteger.ONE;
        for (int i = 2; i <= value; i++) {
            bigIntegers[i] = bigIntegers[i - 1].add(bigIntegers[i - 2]);
        }

        return bigIntegers[bigIntegers.length - 1];
    }
}
