package fibo;

import commons.Task;

import java.math.BigInteger;

public class TaskFibo implements Task {
    @Override
    public String run(String[] data) {
        return fibo(BigInteger.valueOf(Long.parseLong(data[0]))).toString();
    }

    private BigInteger fibo(BigInteger value) {
        if (value.equals(BigInteger.ZERO) || value.equals(BigInteger.ONE)) {
            return value;
        }

        return fibo(value.subtract(BigInteger.ONE)).add(fibo(value.subtract(BigInteger.TWO)));
    }
}
