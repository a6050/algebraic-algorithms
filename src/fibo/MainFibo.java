package fibo;

import commons.Tester;

import java.io.File;

public class MainFibo {
    public static void main(String[] args) {
        Tester tester = new Tester(new TaskFiboWithTwoElementsArray(), new File("data/fibo").getAbsolutePath());
        tester.runTests();
    }
}