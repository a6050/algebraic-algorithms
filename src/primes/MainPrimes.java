package primes;

import commons.Tester;

import java.io.File;

public class MainPrimes {
    public static void main(String[] args) {
        Tester tester;
        tester = new Tester(new TaskPrimeIteration7(), new File("data/primes").getAbsolutePath());
        tester.runTests();
    }
}