package primes;

import commons.Task;

public class TaskPrimeIteration7 implements Task {

    private int[] primes;

    @Override
    public String run(String[] data) {
        long n = Long.parseLong(data[0]);

        int countPrimes = countPrimes(n);

        System.out.print("n: " + n + ", count primes: " + countPrimes + ". ");

        return Integer.toString(countPrimes);
    }

    private int countPrimes(long n) {
        int q = 0;
        int count = 0;
        primes = new int[(int) Math.floor(n / 2)];
        primes[count++] = 2;
        for (int i = 3; i <= n; i += 2) {
            if (isPrime(i)) {
                primes[count++] = i;
            }
        }
        return count;
    }

    private boolean isPrime(int p) {
        int pSqrt = (int) Math.sqrt(p);
        for (int i = 0; primes[i] <= pSqrt; i++) {
            if (p % primes[i] == 0) {
                return false;
            }
        }
        return true;
    }
}
