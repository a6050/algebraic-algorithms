package primes;

import commons.Task;

public class TaskPrimeIteration3 implements Task {
    @Override
    public String run(String[] data) {
        long n = Long.parseLong(data[0]);

        int countPrimes = countPrimes(n);

        System.out.print("n: " + n + ", count primes: " + countPrimes + ". ");

        return Integer.toString(countPrimes);
    }

    private int countPrimes(long n) {
        int q = 0;
        for (int i = 2; i <= n; i++) {
            if (isPrime(i)) {
                q++;
            }
        }
        return q;
    }

    private boolean isPrime(int p) {
        for (int d = 2; d <= p / 2; d++) {
            if (p % d == 0) {
                return false;
            }
        }
        return true;
    }
}
