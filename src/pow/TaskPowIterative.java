package pow;

import commons.Task;

public class TaskPowIterative implements Task {

    @Override
    public String run(String[] data) {

        double value = Double.parseDouble(data[0]);
        long power = Long.parseLong(data[1]);

        if (power == 0) {
            return "1.0";
        }

        double result = value;
        for(int i = 1; i < power; i++) {
            result = result * value;
        }

        return String.format("%.11f", result);
    }
}
