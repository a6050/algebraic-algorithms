package pow;

import commons.Task;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class TaskPowEffective implements Task {

    @Override
    public String run(String[] data) {

        BigDecimal value = new BigDecimal(data[0]);
        long power = Long.parseLong(data[1]);

        if (power == 0) {
            return "1.0";
        }

        if (power == 1) {
            return new DecimalFormat("#.###########").format(value);
        }

        BigDecimal result = BigDecimal.ONE;

        while(power > 0) {
            if (power % 2 == 1) {
                result = result.multiply(value);
            }
            value = value.multiply(value);
            power = (int)(power / 2);
        };

        return new DecimalFormat("#.###########").format(result);
    }
}
