package pow;

import commons.Task;

import java.text.DecimalFormat;

public class TaskPowMultiple implements Task {

    @Override
    public String run(String[] data) {

        double value = Double.parseDouble(data[0]);
        long power = Long.parseLong(data[1]);

        if (power == 0) {
            return "1.0";
        }

        if (power == 1) {
            return String.format("%.11f", value);
        }

        double result = value;

        int minPow = 2;
        int minPowTmp = minPow;

        do {
            result *= result;
            minPow = minPowTmp;
            minPowTmp *= 2;
        } while (minPowTmp < power);

        for(int i = 0; i < power - minPow; i++) {
            result = result * value;
        }

        return new DecimalFormat("#.###########").format(result);
    }
}
