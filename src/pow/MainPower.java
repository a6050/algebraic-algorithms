package pow;

import commons.Tester;
import java.io.File;

public class MainPower {
    public static void main(String[] args) {
        Tester tester = new Tester(new TaskPowEffective(), new File("data/pow").getAbsolutePath());
        tester.runTests();
    }
}