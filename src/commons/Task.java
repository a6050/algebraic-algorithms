package commons;

public interface Task {
    String run(String[] data);
}